//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import <XCTest/XCTest.h>
#import "VMBoard.h"

@interface VMBoardTests : XCTestCase

@end

@implementation VMBoardTests

- (void)testEmptyBoardCount {
    VMBoard board;
    XCTAssertEqual(0, board.count(VMBoard::Red), @"Should have no discs initially");
    XCTAssertEqual(0, board.count(VMBoard::Yellow), @"Should have no discs initially");
}

- (void)testSingleDiscCount {
    VMBoard board;
    board.setDisc(0, 0, VMBoard::Red);
    board.setDisc(1, 0, VMBoard::Yellow);
    XCTAssertEqual(1, board.count(VMBoard::Red), @"Should have one red disc");
    XCTAssertEqual(1, board.count(VMBoard::Yellow), @"Should have one yellow disc");
}

- (void)testFullBoardCount {
    VMBoard board;
    for (int c = 0; c < VMBoard::Columns; c += 1) {
        for (int r = 0; r < VMBoard::Rows; r += 1) {
            board.setDisc(c, r, VMBoard::Red);
        }
    }
    XCTAssertEqual(VMBoard::Columns*VMBoard::Rows, board.count(VMBoard::Red), @"Should have 6*7 red discs");
    XCTAssertEqual(0, board.count(VMBoard::Yellow), @"Should have no yellow discs");
    XCTAssertTrue(board.full(), @"Board should be full");
}

- (void)testEmptyWinner {
    VMBoard board;
    XCTAssertEqual(VMBoard::Empty, board.winner(), @"Should have no winner on an empty board");
}

@end
