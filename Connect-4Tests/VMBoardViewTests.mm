//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import <XCTest/XCTest.h>
#import "VMBoardView.h"

@interface VMBoardViewTests : XCTestCase

@end

@implementation VMBoardViewTests

- (void)testLocationOfDisc {
    VMBoardView* view = [[VMBoardView alloc] init];
    [view sizeToFit];
    
    CGSize spaceSize = [VMBoardView spaceSize];
    CGPoint location = [view locationOfColumn:0 row:0];
    XCTAssertEqual(location.x, spaceSize.width/2, @"Zeroth column should be on the left");
    XCTAssertEqual(location.y, view.bounds.size.height - spaceSize.height/2, @"Zeroth row should be on the bottom");
    
    location = [view locationOfColumn:3 row:5];
    XCTAssertEqual(location.x, view.bounds.size.width/2, @"Middle column should be on the middle of the view");
}

- (void)testColumnAtLocation {
    VMBoardView* view = [[VMBoardView alloc] init];
    [view sizeToFit];
    
    CGSize size = view.bounds.size;
    CGSize spaceSize = [VMBoardView spaceSize];
    CGPoint location = CGPointMake(size.width/2, size.height);
    XCTAssertEqual([view columnAtLocation:location], 3, @"The middle point should match the 3rd column");
    
    location.x -= spaceSize.width/2 - 1;
    XCTAssertEqual([view columnAtLocation:location], 3, @"Column should not change for off-center locations");
    
    location.x += spaceSize.width/2 - 1;
    XCTAssertEqual([view columnAtLocation:location], 3, @"Column should not change for off-center locationsn");
}

- (void)testRowAtLocation {
    VMBoardView* view = [[VMBoardView alloc] init];
    [view sizeToFit];
    
    CGSize size = view.bounds.size;
    CGSize spaceSize = [VMBoardView spaceSize];
    CGPoint location = CGPointMake(size.width/2, size.height - spaceSize.height/2);
    XCTAssertEqual([view rowAtLocation:location], 0, @"The bottom point should match the zeroth row");
    
    location.y -= spaceSize.height/2 - 1;
    XCTAssertEqual([view columnAtLocation:location], 3, @"Row should not change for off-center locations");
    
    location.y += spaceSize.height/2 - 1;
    XCTAssertEqual([view columnAtLocation:location], 3, @"Row should not change for off-center locationsn");
}

@end
