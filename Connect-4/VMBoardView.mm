//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import "VMBoardView.h"

static const CGSize SpaceSize = {100, 100};


@interface VMBoardView ()
@property(strong, nonatomic) NSArray* spaceImageViews;
@property(strong, nonatomic) NSMutableArray* yellowImageViews;
@property(strong, nonatomic) NSMutableArray* redImageViews;
@property(assign) CGPoint touchLocation;
@end


@implementation VMBoardView

+ (CGSize)spaceSize {
    return SpaceSize;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    [self setup];
    return self;
}

- (void)setup {
    [self createSpaces];
    _redImageViews = [[NSMutableArray alloc] initWithCapacity:VMBoard::Columns * VMBoard::Rows];
    _yellowImageViews = [[NSMutableArray alloc] initWithCapacity:VMBoard::Columns * VMBoard::Rows];
}

- (void)createSpaces {
    // Each space on the board has an image representing it. The images are tiled to form the board.
    UIImage* image = [UIImage imageNamed:@"space"];
    
    NSMutableArray* spaceImageViews = [[NSMutableArray alloc] init];
    for (int i = 0; i < VMBoard::Columns * VMBoard::Rows; i += 1) {
        UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
        [spaceImageViews addObject:imageView];
        [self addSubview:imageView];
    }
    self.spaceImageViews = [spaceImageViews copy];
}

- (CGSize)sizeThatFits:(CGSize)size {
    // Determine the ideal size of the board based on the number of columns and rows
    return CGSizeMake(SpaceSize.width * VMBoard::Columns, SpaceSize.height * VMBoard::Rows);
}

- (void)layoutSubviews {
    if (_spaceImageViews.count == 0)
        return;
    
    // Set the position of each view on the board.
    int currentSpace = 0;
    int currentRed = 0;
    int currentYellow = 0;
    for (int c = 0; c < VMBoard::Columns; c += 1) {
        for (int r = 0; r < VMBoard::Rows; r += 1) {
            CGPoint center = [self locationOfColumn:c row:r];
            UIImageView* spaceImageView = _spaceImageViews[currentSpace++];
            spaceImageView.center = center;
            
            if (_board.disc(c, r) == VMBoard::Red && _redImageViews.count > currentRed) {
                UIImageView* imageView = _redImageViews[currentRed++];
                imageView.center = center;
            } else if (_board.disc(c, r) == VMBoard::Yellow && _yellowImageViews.count > currentYellow) {
                UIImageView* imageView = _yellowImageViews[currentYellow++];
                imageView.center = center;
            }
        }
    }
}

- (CGPoint)locationOfColumn:(int)col row:(int)row {
    CGSize size = self.bounds.size;
    
    CGPoint origin;
    origin.x = size.width/2 - SpaceSize.width * VMBoard::Columns / 2;
    origin.y = size.height;
    
    CGPoint center;
    center.x = origin.x + (col + 0.5f) * SpaceSize.width;
    center.y = origin.y - (row + 0.5f) * SpaceSize.height;
    
    return center;
}

- (int)columnAtLocation:(CGPoint)location {
    CGSize size = self.bounds.size;
    
    CGPoint origin;
    origin.x = size.width/2 - SpaceSize.width * VMBoard::Columns / 2;
    origin.y = size.height;
    
    return roundf((location.x - origin.x) / SpaceSize.width - 0.5f);
}

- (int)rowAtLocation:(CGPoint)location {
    CGSize size = self.bounds.size;
    
    CGPoint origin;
    origin.x = size.width/2 - SpaceSize.width * VMBoard::Columns / 2;
    origin.y = size.height;
    
    return roundf(-(location.y - origin.y) / SpaceSize.height - 0.5f);
}

//-(CGRect)getFrameFromColumn(int)col toRow:(int){
//    CGRect *frame = nil;
//    
//    
//    
//    return *frame;
//}


- (void)setBoard:(VMBoard)board {
    if (_board == board)
        return;
    
    // Remove all existing views
    for (UIView* view in _redImageViews)
        [view removeFromSuperview];
    for (UIView* view in _yellowImageViews)
        [view removeFromSuperview];
    
    _board = board;
    [_redImageViews removeAllObjects];
    [_yellowImageViews removeAllObjects];
    
    // Create an image view for each occupied cell on the board
    UIImage* redImage = [UIImage imageNamed:@"red"];
    UIImage* yellowImage = [UIImage imageNamed:@"yellow"];
    for (int c = 0; c < VMBoard::Columns; c += 1) {
        for (int r = 0; r < VMBoard::Rows; r += 1) {
            if (_board.disc(c, r) != VMBoard::Empty) {
                UIImageView* imageView = [[UIImageView alloc] init];
                if (_board.disc(c, r) == VMBoard::Red) {
                    imageView.image = redImage;
                    [_redImageViews addObject:imageView];
                } else {
                    imageView.image = yellowImage;
                    [_yellowImageViews addObject:imageView];
                }
                [imageView sizeToFit];
                [self addSubview:imageView];
            }
        }
    }
    [self setNeedsLayout];
}

- (void)addDisc:(VMBoard::Disc)disc column:(int)col row:(int)row {
    NSLog(@"Col - %d, Row - %d",col, row);
    
    //check for next avaliable row, mod row to avaliable row
    int animatedRow = 0;
    for(int i = 0; i<=5;i++){
        if (_board.disc(col, animatedRow) != VMBoard::Empty) {
            animatedRow++;
        }
        else{
            break;
        }
    }

    
    UIImageView* imageView = [[UIImageView alloc] init];
    if (disc == VMBoard::Red) {
        imageView.image = [UIImage imageNamed:@"red"];
        [_redImageViews addObject:imageView];
    } else {
        imageView.image = [UIImage imageNamed:@"yellow"];
        [_yellowImageViews addObject:imageView];
    }
    [imageView sizeToFit];
    
    //set initial location to begin animations
    imageView.center = CGPointMake([self locationOfColumn:col row:0].x, 0);
    [self addSubview:imageView];
    
    //stop user interaction so that user does not interupt animation
    self.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.8 animations:^{
        imageView.center = [self locationOfColumn:col row:animatedRow];
        
    }
    completion:^(BOOL finished){
         NSLog(@"Animated Disc!");
        //set the final location and bring user interaction back once animation is complete
        _board.setDisc(col, animatedRow, disc);
        self.userInteractionEnabled = YES;
        
        
        if ([self didPlayerWinWithDisc:disc]) {
            //update game UI
            NSLog(@"WE WON!");
        }
    }];
    
}


-(BOOL)didPlayerWinWithDisc:(VMBoard::Disc)disc{
    BOOL didWin = NO;
    int didColCount = 0;
    //first we check the columns
    for (int c=0; c<6; c++) {
        for (int r=0; r<7; r++) {
            if (_board.disc(c, r) != VMBoard::Empty) {
                didColCount++;
                NSLog(@"Count = %d",didColCount);
                if (didColCount > 3) {
                    //we found 4 in a column
                    didWin = YES;
                    break;
                }
            }
            else {
                didColCount = 0;
            }
        }
        
    }
    
    if (didColCount > 3) {
        NSLog(@"win");
    }
    
    return didWin;
}

@end
