//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#include "VMBoard.h"

const int VMBoard::Columns = 7;
const int VMBoard::Rows = 6;

VMBoard::VMBoard() {
    _discs.resize(Rows * Columns, Empty);
}

VMBoard::Disc VMBoard::disc(int col, int row) const {
    return _discs[col * Rows + row];
}

void VMBoard::setDisc(int col, int row, Disc disc) {
    _discs[col * Rows + row] = disc;
}

void VMBoard::clear() {
    for (int i = 0; i < Rows*Columns; i += 1) {
        _discs[i] = Empty;
    }
}

int VMBoard::count(Disc disc) const {
    int count = 0;
    for (int i = 0; i < Rows*Columns; i += 1) {
        if (_discs[i] == disc)
            count += 1;
    }
    return count;
}

VMBoard::Disc VMBoard::winner() const {
    // TODO
    return Empty;
}

bool VMBoard::operator==(const VMBoard& board) const {
    return _discs == board._discs;
}
