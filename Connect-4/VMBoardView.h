//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import <UIKit/UIKit.h>
#import "VMBoard.h"

/**
 VMBoardView displays a Connect-4 board. You can either use `setBoard:` to update the whole view at once or,
 preferably, you can add one disc at a time with `addDisc:column:row`.
 */
@interface VMBoardView : UIView

/**
 The size of each board space.
 */
+ (CGSize)spaceSize;

/**
 The board that is being displayed. Setting this property is expensive use the addDisk: method when possible.
 */
@property(nonatomic) VMBoard board;

/**
 Get the location of a disc within the view. Row 0 is at the botttom.
 @param col The disc column.
 @param row The disc row.
 */
- (CGPoint)locationOfColumn:(int)col row:(int)row;

/**
 Get the disc column at a location within the view. Note that this method can return invalid column values if called
 with a location that lies outside the board.
 @param location The location in view coordinates.
 */
- (int)columnAtLocation:(CGPoint)location;

/**
 Get the disc row at a location within the view. Note that this method can return invalid row values if called with a
 location that lies outside the board.
 @param location The location in view coordinates.
 */
- (int)rowAtLocation:(CGPoint)location;

/**
 Add a disc to the view. This method has no logic, therefore a disc will be added even if there is a disc already at
 that location.
 @param disc The type of disc to add, can be wither VMBoard::Red or VMBoard::Blue.
 @param col The column to  put the disc in.
 @param row The row to put the disc in.
 */
- (void)addDisc:(VMBoard::Disc)disc column:(int)col row:(int)row;

@end
