//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import <UIKit/UIKit.h>
#import "VMBoard.h"

@class VMBoardView;


/**
 VMBoardViewController takes input from the user, updates the board model object and manages the views.
 */
@interface VMBoardViewController : UIViewController

@property(weak, nonatomic) IBOutlet UILabel* statusLabel;
@property(weak, nonatomic) IBOutlet UIImageView* nextDiscImageView;
@property(weak, nonatomic) IBOutlet VMBoardView* boardView;

@property(nonatomic) VMBoard board;
@property(nonatomic) int playerToPlay;

/**
 Handle tap gestures on the board.
 */
- (IBAction)tapGesture:(UITapGestureRecognizer*)gestureRecognizer;

/**
 Restart the game by clearing the board and updating the status.
 */
- (IBAction)restart;

@end
