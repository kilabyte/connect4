//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#import "VMBoardViewController.h"
#import "VMBoardView.h"
#import "VMBoard.h"

@interface VMBoardViewController ()

@end

@implementation VMBoardViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle {
    self = [super initWithNibName:nibName bundle:nibBundle];
    if (!self)
        return nil;
    
    _playerToPlay = 1;
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self)
        return nil;
    
    _playerToPlay = 1;
    return self;
}

- (IBAction)tapGesture:(UITapGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        CGPoint location = [gestureRecognizer locationInView:_boardView];
        [self addDiscAtLocation:location];
    }
}

- (void)addDiscAtLocation:(CGPoint)location {
    int col = [_boardView columnAtLocation:location];
    if (col < 0 || col >= VMBoard::Columns)
        return;
    
    int row = [_boardView rowAtLocation:location];
    if (row < 0 || row >= VMBoard::Rows)
        return;
    
    //we dont need this anymore since we are checking for space avaliability
//    // Avoid adding a dic on top of an existing disc
//    if (_board.disc(col, row) != VMBoard::Empty)
//        return;

    
    // Based on the current player, add a new disc to the board and switch to the next player
    if (_playerToPlay == 1) {
        _board.setDisc(col, row, VMBoard::Red);
        [_boardView addDisc:VMBoard::Red column:col row:row];
        _playerToPlay = 2;
    } else {
        _board.setDisc(col, row, VMBoard::Yellow);
        [_boardView addDisc:VMBoard::Yellow column:col row:row];
        _playerToPlay = 1;
    }
    
    // Update the view
    [self updateStatus];
}

- (void)updateStatus {
    NSString* statusText;
    UIImage* statusImage;
    
    if (_board.full()) {
        statusText = @"Game Over";
        statusImage = nil;
    }
    else if (_playerToPlay == 1) {
        statusText = @"Player 1's turn";
        statusImage = [UIImage imageNamed:@"red"];
    } else {
        statusText = @"Player 2's turn";
        statusImage = [UIImage imageNamed:@"yellow"];
    }
    
    self.statusLabel.text = statusText;
    self.nextDiscImageView.image = statusImage;
}

- (IBAction)restart {
    _board.clear();
    _boardView.board = _board;
    _playerToPlay = 1;
    [self updateStatus];
}

@end
