//  Created by Alejandro Isaza on 2014-03-28.
//  Copyright (c) 2014 Venture Media Labs. All rights reserved.

#pragma once
#include <vector>

/**
 VMBoard is the model of a Connect-4 board. It maintains the current status of the board and provides accessor and
 modifier methods. The board is a 2-dimensional structure with a fixed number of rows and columns. Column 0 is at the
 left, increasing to the right and row 0 is at the bottom increasing up.
 */
class VMBoard {
public:
    /** The number of columns on the board. */
    static const int Columns;
    
    /** The number of rows on the board. */
    static const int Rows;
    
    /** A disc space on the board can be empty, have a red piece or have a yellow piece. */
    enum Disc {
        Empty,
        Red,
        Yellow
    };
    
public:
    VMBoard();
    
    /**
     Get the disc at a specific column and row. The behaviour is undefined if the column or row are outside the board.
     @param col The column in the range [0, Columns-1].
     @param row The row in the range [0, Rows-1].
     */
    Disc disc(int col, int row) const;
    
    /**
     Set a disc at a specific column and row. The behaviour is undefined if the column or row are outside the board.
     @param col The column in the range [0, Columns-1].
     @param row The row in the range [0, Rows-1].
     */
    void setDisc(int col, int row, Disc disc);
    
    /**
     Clear the board.
     */
    void clear();
    
    /**
     Count the number of discs on the board, for a particular type of disc.
     @param disc The type of disc to count.
     */
    int count(Disc disc) const;
    
    /**
     Determine if the board is full.
     */
    bool full() const {
        return count(Empty) == 0;
    }
    
    /**
     Determine the winner.
     @return The type of disc for the current winner or Empty if there is no winner.
     */
    Disc winner() const;
    
    /** Equality operator.
     @return `true` if the boards have the same disc at every location, `false` otherwise.
     */
    bool operator==(const VMBoard& board) const;
    
private:
    std::vector<Disc> _discs;
};
