# Connect 4

Connect 4 is a two-player game where each player takes turns placing a coloured disk on a 7-column by 6-row board. Each player has their own colour. Disks always go on the bottommost available row. The player who first lines up 4 of their discs on a vertical, horizontal or diagonal line wins. For more information see the [Wikipedia page](http://en.wikipedia.org/wiki/Connect_Four).


## Current status

The game currently displays the board and allows players to make moves. But there are some parts missing:

* The discs currently sit 'on the air', instead of dropping to the bottom.
* There is no win detection, the game just goes on until the board is full.


## This is where you come in

Your job is to fix these problems.
* Write the code necessary to make the discs appear at the bottom of the clicked column.
* Write the code to detect if a player won. If so the status message should change to "Player X is the winner!", replacing X with the actual player number. After a win no further moves should be possible until the restart button is pressed.
* For bonus points animate the disc as it falls from the top of the board to its final position.


## Keep in mind

The purpose of this challenge is to assess your approach and the quality of your code. Use all your best coding practices. Having said that, done is better than perfect. You only have 90 minutes.

The archive includes a local git repository. Please use it to commit individual changes.

At the end of 90 minutes take what you have and compress it into a zip file, including the .git directory.